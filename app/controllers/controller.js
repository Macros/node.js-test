const db = require('../config/db.config.js');
const config = require('../config/config.js');

const axios = require('axios');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer');

const User = db.user;

const transporter = nodemailer.createTransport(config.mail.transport);

exports.signup = (req, res) => {
    let avatar = req.file ? req.file.filename : 'avatar-default.png';

    User.create({
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 8),
        avatar: avatar
    }).then(user => {
        return res.send({
            accessToken: jwt.sign({ id: user.id }, config.secret, {
                expiresIn: config.expire_token
            }),
            avatar: config.address + avatar
        });
    }).catch(err => {
        return res.status(500).send({error: err});
    })
}

exports.signin = (req, res) => {
    User.findOne({
        where: {
            email: req.body.email
        }
    }).then(user => {
        if (!user) {
            return res.status(404).send({error: 'User not found'});
        }

        let passwordIsValid = bcrypt.compareSync(req.body.password, user.password);

        if (!passwordIsValid) {
            return res.status(401).send({error: 'Invalid Password'});
        }

        return res.status(200).send({
            user: {
                id: user.id,
                email: user.email,
                avatar: config.address + user.avatar,
            },
            accessToken: jwt.sign({ id: user.id }, config.secret, {
                expiresIn: config.expire_token
            })
        });

    }).catch(err => {
        return res.status(500).send({error: err});
    });
}

exports.sendMail = (req, res) => {
    let names = Array.isArray(req.body.name) ? req.body.name : [req.body.name];
    let profileRequests = names.map((name) => {
        return axios
            .get('https://api.github.com/users/' + name, {
                headers: {
                    'Authorization': 'Bearer ' + config.github_token
                }
            })
            .then(async function (response) {
                let data = {
                    name: response.data.login,
                    email: response.data.email,
                    location: response.data.location,
                };

                if (response.data.location) {
                    data.weather = await axios
                        .get('http://api.openweathermap.org/data/2.5/weather?q=' + response.data.location + '&APPID=' + config.openweathermap_appid)
                        .then(function (response) {
                            return response.data;
                        })
                        .catch(error => null);
                }

                return data;
            })
            .catch(error => ({
                name: name,
                success: false,
                description: 'User not found'
            }));
    });

    axios.all(profileRequests).then(axios.spread((...responses) => {
        res.status(200).send(responses.map((response) => {
            if (response.email) {
                let text = req.body.emailtext;


                if (response.weather) {
                    response.description = 'Message with signature queued for delivery';
                    text = text + "\n-------------------------------\nWeather: "
                        + response.weather.weather[0].main
                        + '; Temp: ' + parseInt(response.weather.main.temp - 273.15) + '°C';
                } else {
                    response.description = 'Message queued for delivery';
                }

                let mailOptions = {
                    from: 'Test <' + req.user.email + '>',
                    subject: 'There is "test" email from <' + req.user.email + '>',
                    to: response.email,
                    text: text
                };

                transporter
                    .sendMail(mailOptions)
                    .then(function (info) {
                        console.log(info);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            } else {
                response.success = false;
                response.description = 'User has no public email';
            }

            return response;
        }));
    }));
}
