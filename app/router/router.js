const request = require('../middleware/request');
const auth = require('../middleware/auth');

module.exports = function(app, upload) {
    const controller = require('../controllers/controller.js');

    app.post('/api/auth/signup', [request.email, request.password, auth.free_email], controller.signup);
    app.post('/api/auth/signin', [request.email, request.password], controller.signin);
    app.post('/api/sendmail', [auth.token, request.name, request.emailtext], controller.sendMail);
}
