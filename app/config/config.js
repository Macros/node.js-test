const env = require('./env.js');
// todo: refactoring env
module.exports = {
    secret: '', // fill
    expire_token: 86400,
    github_token: '', // fill
    openweathermap_appid: '', // fill
    mail: {
        transport: {
            service: 'gmail',
            auth: {
                user: '', // fill
                pass: '' // fill
            }
        },
    },
    min_password_length: 6,
    server: env.server,
    address: env.server.host + ':' + env.server.port + '/',
    avatar_dir: 'uploads/'
};
