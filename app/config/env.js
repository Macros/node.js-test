const env = {
    database: 'nodejs',
    username: 'nodejs',
    password: '', // fill
    host: 'localhost',
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    server: {
        host: '', // fill
        port: 3000
    }
};

module.exports = env;
