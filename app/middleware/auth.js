const jwt = require('jsonwebtoken');
const config = require('../config/config.js');
const db = require('../config/db.config.js');

const User = db.user;

module.exports = {
    token: (req, res, next) => {
        let token = req.headers['authorization'];

        if (!token) {
            return res.status(403).send({ 
                error: 'No token provided'
            });
        }

        token = token.split(' ');

        if (token.length != 2 || token[0] != 'Bearer') {
            return res.status(403).send({ 
                error: 'Undefined token type'
            });
        }

        jwt.verify(token[1], config.secret, async (err, decoded) => {
            if (err) {
                return res.status(500).send({
                    error: 'Fail to authentication'
                });
            }

            req.user = await User.findOne({
                where: {
                    id: decoded.id
                }
            }).then(user => {
                return user.dataValues;
            }).catch(err => {
                return res.status(404).send({ 
                    error: 'User not found'
                });
            });
            
            next();
        });
    },
    free_email: (req, res, next) => {
        User.findOne({
            where: {
                email: req.body.email
            }
        })
        .then(user => {
            if (user) {
                return res.status(400).send({
                    error: 'Email is already taken'
                });
            }

            next();
        });
    }
};
