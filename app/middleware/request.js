const db = require('../config/db.config.js');
const config = require('../config/config.js');
//todo: write one function
module.exports = {
    email: (req, res, next) => {
        if (req.body.email === undefined || req.body.email.length == 0)
            return res.status(400).send({error: 'Email is required field'});
        else {
            let regexp = new RegExp('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}');
            if (!regexp.test(req.body.email))
                return res.status(400).send({error: 'Email is invalid'});
        }

        next();
    },
    password: (req, res, next) => {
        if (req.body.password === undefined || req.body.password.length == 0)
            return res.status(400).send({error: 'Password is required field'});
        else if (req.body.password.length < config.min_password_length)
            return res.status(400).send({error: 'Minimum password length is ' + config.min_password_length + ' characters'});
        next();
    },
    name: (req, res, next) => {
        if (req.body.name === undefined || req.body.name.length == 0)
            return res.status(400).send({error: 'Name is required field'});
        next();
    },
    emailtext: (req, res, next) => {
        if (req.body.emailtext === undefined || req.body.emailtext.length == 0)
            return res.status(400).send({error: 'Emailtext is required field'});
        next();
    }
};
