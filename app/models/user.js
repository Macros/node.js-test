module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define('users', {
        id: {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true
        },
        email: {
            type: Sequelize.STRING(64),
            unique: true
        },
        password: {
            type: Sequelize.STRING(255)
        },
        avatar: {
            type: Sequelize.STRING(40)
        }
    }, {timestamps: false});

    return User;
}
