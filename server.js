var express = require('express');
var app = express();
var multer = require('multer');

const db = require('./app/config/db.config.js');
const config = require('./app/config/config.js');

var avatar_storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.avatar_dir)
	},
	filename: function (req, file, cb) {
		let ext = {
			'image/jpeg': '.jpeg',
			'image/jpg': '.jpg',
			'image/png': '.png',
		};

		ext = ext[file.mimetype] || '.jpg';

		cb(null, file.fieldname + '-' + Date.now() + ext)
	}
})


var upload = multer({ storage: avatar_storage });

app.use(upload.single('avatar')); 
app.use(express.static('uploads'));

require('./app/router/router.js')(app, upload);



db.sequelize.sync({force: true}).then(() => {
    console.log('Drop and Resync with { force: true }');
    initial();
});

var server = app.listen(config.server.port, function () {
    var host = server.address().address
    var port = server.address().port
    console.log("App listening at http://%s:%s", host, port)
});

function initial() {
	//
}
